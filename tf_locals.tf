locals {
  tags_module = {
    Terraform               = true
    Terraform_Module        = "terraform-aws-account-identity"
    Terraform_Module_Source = "https://gitlab.com/tecracer-intern/terraform-landingzone/modules/terraform-aws-account-identity"
    Stage                   = var.stage
    System                  = var.system
  }
  tags = merge(local.tags_module, var.tags)
}
