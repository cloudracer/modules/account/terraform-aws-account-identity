# Creates a group that lets users manage their own Credentials & MFA

resource "aws_iam_group" "user_credentials" {
  count = var.create_manage_own_credentials_group && var.use_iam_access ? 1 : 0

  name = "${replace(replace(replace(var.general_group_naming_construct, "<ACCOUNT>", lower(var.account_name)), "<SYSTEM>", "useraccess"), "<ACCESSLEVEL>", "manageowncredentials")}-tflz"
  path = "/"
}

resource "aws_iam_policy" "user_credentials" {
  count = var.create_manage_own_credentials_group && var.use_iam_access ? 1 : 0

  name        = "${replace(replace(replace(var.general_policy_naming_construct, "<ACCOUNT>-", ""), "<SYSTEM>", "useraccess"), "<ACCESSLEVEL>", "manageowncredentials")}-tflz"
  description = "Allows the user to manage his own credentials"
  path        = "/"
  policy      = templatefile("${path.module}/templates/iam/manage_own_credentials.tpl", {})
}

resource "aws_iam_policy" "user_mfa" {
  count = var.create_manage_own_credentials_group && var.use_iam_access ? 1 : 0

  name        = "${replace(replace(replace(var.general_policy_naming_construct, "<ACCOUNT>-", ""), "<SYSTEM>", "useraccess"), "<ACCESSLEVEL>", "manageownmfa")}-tflz"
  description = "Allows to manage own MFA"
  path        = "/"
  policy      = templatefile("${path.module}/templates/iam/manage_own_mfa.tpl", {})
}

resource "aws_iam_group_policy_attachment" "user_credentials" {
  count = var.create_manage_own_credentials_group && var.use_iam_access ? 1 : 0

  group      = aws_iam_group.user_credentials[0].name
  policy_arn = aws_iam_policy.user_credentials[0].arn
}

resource "aws_iam_group_policy_attachment" "user_mfa" {
  count = var.create_manage_own_credentials_group && var.use_iam_access ? 1 : 0

  group      = aws_iam_group.user_credentials[0].name
  policy_arn = aws_iam_policy.user_mfa[0].arn
}
