# Account Core Module

The account core module is to be rolled out in every AWS Account.

It basically create IAM roles that can be accessed from one or more
principals that are declared in an input variable. Examples are other
AWS accounts (user mananagement account) or a federated identity
provider such as Microsoft Active Directory.

Roles are defined by role policy attachments (RPAs). The module will
create a new role for every distinct role name in the RPA variable.

Policies that shall be attached to these roles are identified by ARNs
and need to be created outside the module. A future extension of this
module might include dynamic policy creation.

By default, it will create a AdminRole and a ReadOnlyRole. These can
be disabled by setting standard_role_policy_attachments = false.

Furthermore, service roles for AWS Config and others are created.

## Contributing
This module is intended to be a shared module.
Please don't commit any customer-specific configuration into this module and keep in mind that changes could affect other projects.

For new features please create a branch and submit a pull request to the maintainers.

Accepted new features will be published within a new release/tag.

## Pre-Commit Hooks

### Enabled hooks
- id: end-of-file-fixer
- id: trailing-whitespace
- id: terraform_fmt
- id: terraform_docs
- id: terraform_validate
- id: terraform_tflint

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.2.0 |
| <a name="provider_aws.shared"></a> [aws.shared](#provider\_aws.shared) | 4.2.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_assumable_role_sets"></a> [assumable\_role\_sets](#module\_assumable\_role\_sets) | ./modules/role_set | n/a |
| <a name="module_aws_sso_permission_attachement"></a> [aws\_sso\_permission\_attachement](#module\_aws\_sso\_permission\_attachement) | ./modules/sso_permissions | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_iam_account_alias.alias](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_account_alias) | resource |
| [aws_iam_account_password_policy.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_account_password_policy) | resource |
| [aws_iam_group.user_credentials](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group) | resource |
| [aws_iam_group_policy_attachment.user_credentials](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group_policy_attachment) | resource |
| [aws_iam_group_policy_attachment.user_mfa](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group_policy_attachment) | resource |
| [aws_iam_instance_profile.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile) | resource |
| [aws_iam_policy.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.user_credentials](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.user_mfa](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_role.aws_support_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_ssm_parameter.account_id](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_iam_policy.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy) | data source |
| [aws_iam_policy_document.assume_from_account](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.assume_role_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_account_alias"></a> [account\_alias](#input\_account\_alias) | Specifies the name of the account. | `string` | `null` | no |
| <a name="input_account_name"></a> [account\_name](#input\_account\_name) | Name of the current account. | `string` | n/a | yes |
| <a name="input_aws_organizations_account_resource"></a> [aws\_organizations\_account\_resource](#input\_aws\_organizations\_account\_resource) | The AWS Orginzations resource for this account | `any` | `{}` | no |
| <a name="input_create_manage_own_credentials_group"></a> [create\_manage\_own\_credentials\_group](#input\_create\_manage\_own\_credentials\_group) | Wheter a Manage own Credentials group should be created | `bool` | `true` | no |
| <a name="input_general_group_naming_construct"></a> [general\_group\_naming\_construct](#input\_general\_group\_naming\_construct) | Defines the Naming of AWS IAM Groups. You need to add: `<ACCOUNT>`, `<SYSTEM>` and `<ACCESSLEVEL>` in the string! We will always add -tflz at the end. | `string` | n/a | yes |
| <a name="input_general_policy_naming_construct"></a> [general\_policy\_naming\_construct](#input\_general\_policy\_naming\_construct) | Defines the Naming of AWS IAM Policies. You need to add: `<ACCOUNT>`, `<SYSTEM>` and `<ACCESSLEVEL>` in the string! We will always add -tflz at the end. | `string` | n/a | yes |
| <a name="input_general_role_naming_construct"></a> [general\_role\_naming\_construct](#input\_general\_role\_naming\_construct) | Defines the Naming of AWS IAM Roles. You need to add: `<SYSTEM>` and `<ACCESSLEVEL>` in the string! We will always add -tflz at the end. | `string` | n/a | yes |
| <a name="input_iam_max_password_age"></a> [iam\_max\_password\_age](#input\_iam\_max\_password\_age) | Maximum age of IAM User Password for AWS Account | `number` | `90` | no |
| <a name="input_iam_minimum_password_length"></a> [iam\_minimum\_password\_length](#input\_iam\_minimum\_password\_length) | Minimum length of IAM User Password for AWS Account. | `number` | `16` | no |
| <a name="input_iam_password_reuse_prevention"></a> [iam\_password\_reuse\_prevention](#input\_iam\_password\_reuse\_prevention) | Number of previous IAM User Passwords for AWS Account that can't be used again | `number` | `24` | no |
| <a name="input_iam_role_max_session_duration"></a> [iam\_role\_max\_session\_duration](#input\_iam\_role\_max\_session\_duration) | Maximum duration that a User can have credentials for an IAM Role | `number` | `43200` | no |
| <a name="input_iam_role_use_mfa"></a> [iam\_role\_use\_mfa](#input\_iam\_role\_use\_mfa) | If the Role can only be assume with MFA activated or not | `bool` | `true` | no |
| <a name="input_iam_store_role_in_ssm"></a> [iam\_store\_role\_in\_ssm](#input\_iam\_store\_role\_in\_ssm) | If the role information should be store in SSM Parameter Store. | `bool` | `true` | no |
| <a name="input_principal"></a> [principal](#input\_principal) | ARN of accounts, groups, or users with the ability to assume this role. | `string` | `null` | no |
| <a name="input_region"></a> [region](#input\_region) | The region of the account | `string` | n/a | yes |
| <a name="input_roles_definition"></a> [roles\_definition](#input\_roles\_definition) | A JSON defintion for the roles in each account. Use like {role = {short\_name: x, system: y, managed\_policy\_arns: [], custom\_role\_policies: {}}} | `map` | `{}` | no |
| <a name="input_sso_permissions"></a> [sso\_permissions](#input\_sso\_permissions) | A list of maps, describing the SSO Permission Sets (e.g. {group: Admins, permission\_sets: [a, b, c]}) | `list` | `[]` | no |
| <a name="input_stage"></a> [stage](#input\_stage) | Name of a dedicated system or application | `string` | n/a | yes |
| <a name="input_system"></a> [system](#input\_system) | Name of a dedicated system or application | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Tag that should be applied to all resources. | `map(string)` | `{}` | no |
| <a name="input_use_iam_access"></a> [use\_iam\_access](#input\_use\_iam\_access) | If User Access via IAM should be used | `bool` | `false` | no |
| <a name="input_use_sso_access"></a> [use\_sso\_access](#input\_use\_sso\_access) | If Access via AWS SSO should be used | `bool` | `false` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
