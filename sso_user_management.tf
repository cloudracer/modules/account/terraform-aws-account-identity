# Input looks like: [{"group":"aws-bt-GlobalAdministrators","permission_sets":["AdministratorAccess"]},{"permission_sets":["AdministratorAccess"],"user":"smoehn@debertrandt.onmicrosoft.com"}]

locals {
  sso_permissions_as_map = { for permission in var.sso_permissions : "${substr(tostring(md5(jsonencode(permission))), 2, 8)}_${data.aws_caller_identity.current.account_id}" => permission } # Create a unique ID for the Terraform State
}

// SSO Module
module "aws_sso_permission_attachement" {
  source   = "./modules/sso_permissions"
  for_each = local.sso_permissions_as_map

  providers = {
    aws = aws.sso
  }

  sso_permission    = each.value
  target_account_id = data.aws_caller_identity.current.account_id
}
