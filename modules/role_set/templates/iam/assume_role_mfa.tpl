{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "${PRINCIPAL_TYPE}": "${PRINCIPAL}"
      },
      "Effect": "Allow",
      "Condition": {"BoolIfExists": {"aws:MultiFactorAuthPresent": "true"}}
    }
  ]
}
