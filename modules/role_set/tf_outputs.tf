output "role_name" {
  value       = aws_iam_role.role.name
  description = "Name of the Role"
}
