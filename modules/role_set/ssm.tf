// Save Role Information in SSM in a shared Account
resource "aws_ssm_parameter" "account_role" {
  count    = var.store_role_in_ssm ? 1 : 0
  provider = aws.shared

  name        = "/accounts/${replace(var.account_name, " ", "_")}/roles/${aws_iam_role.role.name}"
  description = "Information about this role."
  type        = "SecureString"
  value = jsonencode({
    name : aws_iam_role.role.name,
    arn : aws_iam_role.role.arn,
    max_session_duration : aws_iam_role.role.max_session_duration
  })

  tags = var.tags
}
