locals {
  assume_role_policy = var.role_use_mfa ? "${path.module}/templates/iam/assume_role_mfa.tpl" : "${path.module}/templates/iam/assume_role.tpl"
}

# --
# -- Create the Role and the Policies for the Role -- #
# --

// Create IAM Role in Account that can be assumed from var.assume_from_account
resource "aws_iam_role" "role" {
  name                 = var.role_name
  max_session_duration = var.role_max_session_duration
  assume_role_policy = templatefile(local.assume_role_policy, {
    PRINCIPAL_TYPE = "AWS",
    PRINCIPAL      = var.assumable_from_account_id
  })
  tags = var.tags

  force_detach_policies = true
  lifecycle {
    create_before_destroy = true
  }
}

// Managed Policies: Attach one or multiple managed Policies to the role
resource "aws_iam_role_policy_attachment" "managed_role_policies" {
  for_each = toset(var.managed_policy_arns)

  role       = aws_iam_role.role.name
  policy_arn = each.value

  depends_on = [aws_iam_role.role]
}

# Custom Policies: Create policies
resource "aws_iam_policy" "custom_role_policies" {
  for_each = var.custom_role_policies

  name   = each.value.name
  policy = jsonencode(each.value.policy)
}

# Custom Policies: Attach Policy to the main Role
resource "aws_iam_role_policy_attachment" "attachements" {
  for_each = var.custom_role_policies

  role       = aws_iam_role.role.name
  policy_arn = aws_iam_policy.custom_role_policies[each.key].arn

  depends_on = [aws_iam_role.role, aws_iam_policy.custom_role_policies]
}

# --
# -- Create the Group in the User Account and the according Policy -- #
# --

# Create Group in the User Account
resource "aws_iam_group" "group" {
  provider = aws.user

  name = var.user_account_group_name
  path = "/"
}

# Create policy in USER Account
resource "aws_iam_policy" "assume_role_policy" {
  provider = aws.user

  name = var.user_account_assume_role_policy_name

  policy = templatefile("${path.module}/templates/iam/assume_role_resource.tpl", {
    RESOURCE = aws_iam_role.role.arn
  })
}

# Attach Assume Policy to the Group
resource "aws_iam_group_policy_attachment" "attachement" {
  provider = aws.user

  group      = aws_iam_group.group.name
  policy_arn = aws_iam_policy.assume_role_policy.arn

  depends_on = [aws_iam_group.group, aws_iam_policy.assume_role_policy]
}
