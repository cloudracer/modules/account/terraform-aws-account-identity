variable "account_name" {
  type        = string
  description = "Name of the current account."
}

variable "role_name" {
  type        = string
  description = "Name for the Role to be created"
}

variable "role_max_session_duration" {
  type        = number
  description = "Maximum duration that a User can have credentials for the IAM Role"
  default     = 43200
}

variable "role_use_mfa" {
  type        = bool
  description = "If the Role can only be assume with MFA activated or not"
  default     = true
}

variable "assumable_from_account_id" {
  type        = string
  description = "Account ID from which the role can be assumed"
}

variable "managed_policy_arns" {
  type        = list(string)
  description = "A list of Policy ARNs that should be added to the role"
  default     = []
}

variable "custom_role_policies" {
  description = "A Map with Maps that describe custom Policies that should be added to the role. (e.g. {pol1 = {name: name, policy: policy}, pol2 = {name, policy}})"
  default     = {}
}

variable "user_account_group_name" {
  type        = string
  description = "Name for the Group that will be crated in the Users Account"
}

variable "user_account_assume_role_policy_name" {
  type        = string
  description = "Name for the Policy that will be crated in the Users Account"
}

variable "store_role_in_ssm" {
  type        = bool
  description = "If the role information should be store in SSM Parameter Store."
  default     = true
}

variable "tags" {
  description = "Map of Tags that should be applied to all resources"
}
