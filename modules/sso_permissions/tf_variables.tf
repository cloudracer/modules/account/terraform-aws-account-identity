variable "sso_permission" {
  description = "Object with Permission information"
}

variable "target_account_id" {
  description = "AWS Account ID for the Account that should be accessible"
}
