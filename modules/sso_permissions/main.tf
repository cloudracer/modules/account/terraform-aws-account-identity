## Info
## var.sso_permission looks like: { "group" : "Administrators", "permission_sets" : ["AdministratorAccess", "ReadOnlyAccess"] }
## var.sso_permission can also look like: { "groups" : ["Administrators", "Developers"], "permission_sets" : ["AdministratorAccess", "ReadOnlyAccess"] }

// Single Group or User (old style) and multiple permissions --> Keep for legacy reasons!
data "aws_identitystore_group" "idp" {
  count = lookup(var.sso_permission, "group", null) != null ? 1 : 0

  identity_store_id = tolist(data.aws_ssoadmin_instances.sso.identity_store_ids)[0]

  filter {
    attribute_path  = "DisplayName"
    attribute_value = var.sso_permission.group
  }
}

data "aws_identitystore_user" "idp" {
  count = lookup(var.sso_permission, "user", null) != null ? 1 : 0

  identity_store_id = tolist(data.aws_ssoadmin_instances.sso.identity_store_ids)[0]

  filter {
    attribute_path  = "UserName"
    attribute_value = var.sso_permission.user
  }
}

resource "aws_ssoadmin_account_assignment" "sso_assignment" {
  for_each = lookup(var.sso_permission, "group", null) != null || lookup(var.sso_permission, "user", null) != null ? toset(var.sso_permission.permission_sets) : []

  instance_arn       = data.aws_ssoadmin_permission_set.permission_sets[each.key].instance_arn
  permission_set_arn = data.aws_ssoadmin_permission_set.permission_sets[each.key].arn

  principal_id   = lookup(var.sso_permission, "group", null) != null ? data.aws_identitystore_group.idp[0].group_id : data.aws_identitystore_user.idp[0].user_id
  principal_type = lookup(var.sso_permission, "group", null) != null ? "GROUP" : "USER"

  target_id   = var.target_account_id
  target_type = "AWS_ACCOUNT"
}

// Multiple Groups or Users (new style) and multiple permissions
locals {
  permission_sets_as_map = lookup(var.sso_permission, "permission_sets", null) != null ? { for permission_set in var.sso_permission["permission_sets"] : md5(permission_set) => permission_set } : {}
  groups_as_map          = lookup(var.sso_permission, "groups", null) != null ? { for group in var.sso_permission["groups"] : md5(group) => group } : {}
  users_as_map           = lookup(var.sso_permission, "users", null) != null ? { for user in var.sso_permission["users"] : md5(user) => user } : {}
}

data "aws_ssoadmin_instances" "sso" {}

data "aws_ssoadmin_permission_set" "permission_sets" {
  for_each = toset(var.sso_permission.permission_sets)

  instance_arn = tolist(data.aws_ssoadmin_instances.sso.arns)[0]
  name         = each.value
}

data "aws_identitystore_group" "idp_multiple" {
  for_each = local.groups_as_map

  identity_store_id = tolist(data.aws_ssoadmin_instances.sso.identity_store_ids)[0]

  filter {
    attribute_path  = "DisplayName"
    attribute_value = each.value
  }
}

data "aws_identitystore_user" "idp_multiple" {
  for_each = local.users_as_map

  identity_store_id = tolist(data.aws_ssoadmin_instances.sso.identity_store_ids)[0]

  filter {
    attribute_path  = "UserName"
    attribute_value = each.value
  }
}

locals {
  permission_as_list = lookup(var.sso_permission, "permission_sets", null) != null ? [for permission_set in data.aws_ssoadmin_permission_set.permission_sets : permission_set.id] : []
  groups_as_list     = lookup(var.sso_permission, "groups", null) != null ? [for group in data.aws_identitystore_group.idp_multiple : group.id] : []
  users_as_list      = lookup(var.sso_permission, "users", null) != null ? [for user in data.aws_identitystore_user.idp_multiple : user.id] : []

  permissions_groups_product = length(local.groups_as_list) > 0 ? { for perm in setproduct(local.permission_as_list, local.groups_as_list) : md5("${perm[0]}_${perm[1]}") => perm } : {}
  permissions_users_product  = length(local.users_as_list) > 0 ? { for perm in setproduct(local.permission_as_list, local.users_as_list) : md5("${perm[0]}_${perm[1]}") => perm } : {}
}

resource "aws_ssoadmin_account_assignment" "sso_assignment_multiple_groups" {
  for_each = local.permissions_groups_product

  instance_arn = tolist(data.aws_ssoadmin_instances.sso.arns)[0]

  permission_set_arn = each.value[0]
  principal_id       = each.value[1]
  principal_type     = "GROUP"

  target_id   = var.target_account_id
  target_type = "AWS_ACCOUNT"
}

resource "aws_ssoadmin_account_assignment" "sso_assignment_multiple_users" {
  for_each = local.permissions_users_product

  instance_arn = tolist(data.aws_ssoadmin_instances.sso.arns)[0]

  permission_set_arn = each.value[0]
  principal_id       = each.value[1]
  principal_type     = "USER"

  target_id   = var.target_account_id
  target_type = "AWS_ACCOUNT"
}
