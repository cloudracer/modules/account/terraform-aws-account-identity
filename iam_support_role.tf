resource "aws_iam_role" "aws_support_role" {
  name        = "AWSSupportAccess" # Do not change this name, this is needed for CIS
  description = "AWS Support Role as per AWS SecurityHub CIS Documentation 1.20"

  assume_role_policy  = data.aws_iam_policy_document.assume_from_account.json
  managed_policy_arns = ["arn:aws:iam::aws:policy/AWSSupportAccess"]

  tags = local.tags
}

data "aws_iam_policy_document" "assume_from_account" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "AWS"
      identifiers = [data.aws_caller_identity.current.account_id]
    }
  }
}
