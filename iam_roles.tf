// Call Module!
module "assumable_role_sets" {
  source   = "./modules/role_set"
  for_each = var.use_iam_access ? var.roles_definition : {}

  providers = {
    aws        = aws
    aws.user   = aws.iam
    aws.shared = aws.shared
  }

  account_name                         = var.account_name
  role_name                            = "${replace(replace(var.general_role_naming_construct, "<SYSTEM>", each.value.system), "<ACCESSLEVEL>", each.value.short_name)}-tflz"
  role_max_session_duration            = var.iam_role_max_session_duration
  role_use_mfa                         = var.iam_role_use_mfa
  assumable_from_account_id            = var.principal
  managed_policy_arns                  = lookup(each.value, "managed_policy_arns", [])
  custom_role_policies                 = lookup(each.value, "custom_role_policies", {})
  user_account_group_name              = "${replace(replace(replace(var.general_group_naming_construct, "<ACCOUNT>", lower(var.account_name)), "<SYSTEM>", each.value.system), "<ACCESSLEVEL>", each.value.short_name)}-tflz"
  user_account_assume_role_policy_name = "${replace(replace(replace(var.general_group_naming_construct, "<ACCOUNT>", lower(var.account_name)), "<SYSTEM>", each.value.system), "<ACCESSLEVEL>", "assume-${each.value.short_name}")}-tflz"
  store_role_in_ssm                    = var.iam_store_role_in_ssm
  tags                                 = local.tags
}
