# Create Role for SSM Usage in every Account
data "aws_iam_policy" "default" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM"
}

resource "aws_iam_instance_profile" "default" {
  name = "ssm-tflz"
  role = aws_iam_role.default.name

  tags = local.tags
}

resource "aws_iam_role" "default" {
  name               = "rl-ssm-tflz"
  description        = "Role that allows EC2 Instanze to be used with AWS Systems Manager"
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy.json

  tags = local.tags
}

data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_policy" "default" {
  name        = "pol-ssm-tflz"
  description = "Policy that allows EC2 Instanze to be used with AWS Systems Manager"
  policy      = data.aws_iam_policy.default.policy

  tags = local.tags
}

resource "aws_iam_role_policy_attachment" "default" {
  role       = aws_iam_role.default.name
  policy_arn = aws_iam_policy.default.arn
}
