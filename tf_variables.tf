// Required for provider
variable "region" {
  type        = string
  description = "The region of the account"
}

variable "aws_organizations_account_resource" {
  type        = any
  description = "The AWS Orginzations resource for this account"
  default     = {}
}

// Organization specific tags

variable "create_manage_own_credentials_group" {
  type        = bool
  description = "Wheter a Manage own Credentials group should be created"
  default     = true
}

variable "account_name" {
  type        = string
  description = "Name of the current account."
}

variable "system" {
  type        = string
  description = "Name of a dedicated system or application"
}

variable "stage" {
  type        = string
  description = "Name of a dedicated system or application"
}

// Tags
variable "tags" {
  type        = map(string)
  description = "Tag that should be applied to all resources."
  default     = {}
}

// Module specific variables

variable "account_alias" {
  type        = string
  description = "Specifies the name of the account."
  default     = null
}

# - AWS SSO - #
variable "use_sso_access" {
  type        = bool
  description = "If Access via AWS SSO should be used"
  default     = false
}

variable "sso_permissions" {
  description = "A list of maps, describing the SSO Permission Sets (e.g. {group: Admins, permission_sets: [a, b, c]})"
  default     = []
}

# - IAM - #
variable "use_iam_access" {
  type        = bool
  description = "If User Access via IAM should be used"
  default     = false
}

variable "roles_definition" {
  description = "A JSON defintion for the roles in each account. Use like {role = {short_name: x, system: y, managed_policy_arns: [], custom_role_policies: {}}}"
  default     = {}
}

variable "principal" {
  type        = string
  description = "ARN of accounts, groups, or users with the ability to assume this role."
  default     = null
}

variable "iam_minimum_password_length" {
  type        = number
  description = "Minimum length of IAM User Password for AWS Account."
  default     = 16
}

variable "iam_max_password_age" {
  type        = number
  description = "Maximum age of IAM User Password for AWS Account"
  default     = 90
}

variable "iam_password_reuse_prevention" {
  type        = number
  description = "Number of previous IAM User Passwords for AWS Account that can't be used again"
  default     = 24
}

variable "iam_role_max_session_duration" {
  type        = number
  description = "Maximum duration that a User can have credentials for an IAM Role"
  default     = 43200
}

variable "iam_role_use_mfa" {
  type        = bool
  description = "If the Role can only be assume with MFA activated or not"
  default     = true
}

variable "iam_store_role_in_ssm" {
  type        = bool
  description = "If the role information should be store in SSM Parameter Store."
  default     = true
}

# IAM
variable "general_role_naming_construct" {
  type        = string
  description = "Defines the Naming of AWS IAM Roles. You need to add: `<SYSTEM>` and `<ACCESSLEVEL>` in the string! We will always add -tflz at the end."
}

variable "general_policy_naming_construct" {
  type        = string
  description = "Defines the Naming of AWS IAM Policies. You need to add: `<ACCOUNT>`, `<SYSTEM>` and `<ACCESSLEVEL>` in the string! We will always add -tflz at the end."
}

variable "general_group_naming_construct" {
  type        = string
  description = "Defines the Naming of AWS IAM Groups. You need to add: `<ACCOUNT>`, `<SYSTEM>` and `<ACCESSLEVEL>` in the string! We will always add -tflz at the end."
}
