# Alternative Account Alias if needed
resource "aws_iam_account_alias" "alias" {
  count         = var.account_alias != null ? 1 : 0
  account_alias = var.account_alias
}

# Store Account Name in SSM
resource "aws_ssm_parameter" "account_id" {
  count    = var.aws_organizations_account_resource == {} ? 0 : 1
  provider = aws.shared

  name        = "/accounts/${replace(var.account_name, " ", "_")}"
  description = "The account ID for this AWS Account"
  type        = "SecureString"
  value = jsonencode({
    name : var.aws_organizations_account_resource.name,
    id : var.aws_organizations_account_resource.id,
    arn : var.aws_organizations_account_resource.arn,
    email : var.aws_organizations_account_resource.email
  })
  tags = local.tags
}

// IAM Password Policy
resource "aws_iam_account_password_policy" "this" {
  minimum_password_length        = var.iam_minimum_password_length
  max_password_age               = var.iam_max_password_age
  password_reuse_prevention      = var.iam_password_reuse_prevention
  require_lowercase_characters   = true
  require_numbers                = true
  require_uppercase_characters   = true
  require_symbols                = true
  allow_users_to_change_password = true
}
